package oleh.kukliak.dropnet.Repository;

import oleh.kukliak.dropnet.Entity.Ads;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdsRepository extends JpaRepository<Ads, Long> {

    List<Ads> findByWholesale_price(Integer from, Integer to);

    List<Ads> findByDrop_price(Integer from, Integer to);

    List<Ads> findByCategory(String category);

    List<Ads> findByLocation(String location);

    List<Ads> findByName_ads(String name_ads);

    List<Ads> findBySabcategory(String sabcategory);

}
