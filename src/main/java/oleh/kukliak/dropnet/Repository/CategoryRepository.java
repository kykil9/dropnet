package oleh.kukliak.dropnet.Repository;

import oleh.kukliak.dropnet.Entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Query("select c from Category c join fetch c.ads ads where c.ads=:cName")
    Category findByName(@Param("cName") String name);
}
