package oleh.kukliak.dropnet.Repository;

import oleh.kukliak.dropnet.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("select u from User u join fetch u.ads ads where u.name=:uName")
    User findByName(@Param("cName") String name);
}
