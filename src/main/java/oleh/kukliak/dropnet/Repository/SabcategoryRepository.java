package oleh.kukliak.dropnet.Repository;

import oleh.kukliak.dropnet.Entity.Sabcategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SabcategoryRepository extends JpaRepository<Sabcategory, Long> {
    @Query("select sab from Sabcategory sab join fetch sab.ads ads where sab.name=:sabName")
    Sabcategory findByName(@Param("sabName") String name);
}
