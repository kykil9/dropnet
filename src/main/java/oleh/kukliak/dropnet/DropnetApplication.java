package oleh.kukliak.dropnet;

import oleh.kukliak.dropnet.Entity.Ads;
import oleh.kukliak.dropnet.Entity.User;
import oleh.kukliak.dropnet.Repository.AdsRepository;
import oleh.kukliak.dropnet.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class   DropnetApplication {

    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    public void init(){
        User user = new User();
        user.setName("Oleh");
        user.setLast_name("Kukliak");
        user.setPhone_number("380676889380");
        user.setEmail("okuklyak@gmail.com");
        user.setYour_business("business");
        user.setPassword("123456");

        User user1 = new User();
        user1.setName("Max");
        user1.setLast_name("Lomachenko");
        user1.setPhone_number("3806789350");
        user1.setEmail("Max@gmail.com");
        user1.setYour_business("business");
        user1.setPassword("456789");

        userRepository.save(user);
        userRepository.save(user1);

    }
    @Autowired
    private AdsRepository adsRepository;

    @PostConstruct
    public void init1(){
        Ads ads = new Ads();
        ads.setName_ads("Мишки из роз");
//        ads.setSabcategory("Подарунки");
        ads.setWholesale_price(20);
        ads.setDrop_price(25);
        ads.setDescription("40см; 1300 троянд; ручна робота");
//        ads.setPhotos();
        ads.setLocation("Львів");
        ads.setPhone_number("380676889380");
        ads.setEmail("okuklyak@gmail.com");
        ads.setPerson_name("Олег");

        adsRepository.save(ads);
    }
    public static void main(String[] args) {
        SpringApplication.run(DropnetApplication.class, args);
    }

}
