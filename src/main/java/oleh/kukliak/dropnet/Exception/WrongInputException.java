package oleh.kukliak.dropnet.Exception;

public class WrongInputException extends Exception {

    public WrongInputException() {
    }

    public WrongInputException(String message) {
        super(message);
    }
}
