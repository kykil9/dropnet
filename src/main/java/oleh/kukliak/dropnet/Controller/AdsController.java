package oleh.kukliak.dropnet.Controller;

import oleh.kukliak.dropnet.Exception.WrongInputException;
import oleh.kukliak.dropnet.Service.AdsService;
import oleh.kukliak.dropnet.dto.Response.AdsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/ads")
public class AdsController {

    @Autowired
    private AdsService adsService;

    @GetMapping
    public List<AdsResponse> findAll(){return adsService.findAll();}

//    @PostMapping
//    public AdsResponse save(@RequestBody AdsResponse carRequest) throws WrongInputException {
//        return adsService.save(carRequest);
//    }
//
//    @PutMapping
//    public AdsResponse Update(@RequestParam Long id, @RequestBody AdsResponse carRequest) throws WrongInputException {
//        return adsService.update(id, carRequest);
//    }
//
//    @DeleteMapping
//    public void delete(@RequestParam Long id) throws WrongInputException {
//        adsService.delete(id);
//    }
}
