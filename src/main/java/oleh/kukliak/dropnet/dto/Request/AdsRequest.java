package oleh.kukliak.dropnet.dto.Request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdsRequest {

    private String name_ads;

    private Long sabcategoryId;

    private Integer wholesale_price;

    private Integer drop_price;

    private String description;

    private String location;

    private String phone_number;

    private String email;

    private String person_name;
}
