package oleh.kukliak.dropnet.dto.Request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {

    private String name;

    private String last_name;

    private String phone_number;

    private String email;

    private String your_business;

    private String password;
}
