package oleh.kukliak.dropnet.dto.Request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SabcategoryRequest {
    private String name;
}
