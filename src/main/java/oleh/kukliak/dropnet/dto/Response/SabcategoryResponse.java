package oleh.kukliak.dropnet.dto.Response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import oleh.kukliak.dropnet.Entity.Category;
import oleh.kukliak.dropnet.Entity.Sabcategory;

@Getter
@Setter
@NoArgsConstructor
public class SabcategoryResponse {
    private Long id;
    private String name;

    public SabcategoryResponse(Sabcategory sabcategory){
        id = sabcategory.getId();
        name = sabcategory.getName();
    }
}