package oleh.kukliak.dropnet.dto.Response;

import lombok.Getter;
import lombok.Setter;
import oleh.kukliak.dropnet.Entity.Ads;

@Getter
@Setter
public class AdsResponse {

    private Long id;

    private String name_ads;

    private String sabcategoryName;

    private Integer wholesale_price;

    private Integer drop_price;

    private String description;

    private String location;

    private String phone_number;

    private String email;

    private String person_name;

    public AdsResponse(Ads ads){
        id = ads.getId();
        name_ads = ads.getName_ads();
        sabcategoryName = ads.getSabcategory().getName();
        wholesale_price = ads.getWholesale_price();
        drop_price = ads.getDrop_price();
        description = ads.getDescription();
        location = ads.getLocation();
        phone_number = ads.getPhone_number();
        email = ads.getEmail();
        person_name = ads.getPerson_name();
    }
}
