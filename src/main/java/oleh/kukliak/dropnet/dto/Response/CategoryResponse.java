package oleh.kukliak.dropnet.dto.Response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import oleh.kukliak.dropnet.Entity.Category;

@Getter
@Setter
@NoArgsConstructor
public class CategoryResponse {
    private Long id;
    private String name;

    public CategoryResponse(Category category){
        id = category.getId();
        name = category.getName();
    }
}
