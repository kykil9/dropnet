package oleh.kukliak.dropnet.dto.Response;

import lombok.Getter;
import lombok.Setter;
import oleh.kukliak.dropnet.Entity.User;

@Getter
@Setter
public class UserResponse {

    private Long id;

    private String name;

    private String last_name;

    private String phone_number;

    private String email;

    private String your_business;

    private String password;

    public UserResponse(User user){
        id = user.getId();
        name = user.getName();
        last_name = user.getLast_name();
        phone_number = user.getPhone_number();
        email = user.getEmail();
        your_business = user.getYour_business();
        password = user.getPassword();
    }
}
