package oleh.kukliak.dropnet.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;
import java.io.File;

@Setter
@Getter
@ToString
@NoArgsConstructor

@Entity
public class Currency {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private File symbol;

    @ManyToOne
    private Ads ads;
}
