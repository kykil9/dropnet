package oleh.kukliak.dropnet.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.File;

@Setter
@Getter
@ToString
@NoArgsConstructor

@Entity
public class Ads {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name_ads;

//    @NotNull
//    private String sabcategory;

    @OneToOne
    private Sabcategory sabcategory;

    @NotNull
    private Integer wholesale_price;

    @OneToMany
    private Currency currency;

    @NotNull
    private Integer drop_price;

//    @OneToMany
//    private Currency currency;

    @NotNull
    private String description;

//    private File photos;

    @OneToOne
    private Photos photos;

    @NotNull
    private String location;

    @NotNull
    @Pattern(regexp="^((\\+38)|38)?0\\d{9}$")

    private String phone_number;

    @NotNull
    @Pattern(regexp=".+@.+\\..+")
    private String email;

    @NotNull
    private String person_name;

    ////////////////////////////
    @ManyToOne
    private User user;

//    @OneToOne
//    private Sabcategory sabcategory;

//    @OneToOne
//    private Photos photos;
}
