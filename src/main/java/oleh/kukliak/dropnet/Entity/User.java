package oleh.kukliak.dropnet.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.management.relation.Role;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@ToString

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String last_name;

    @Column(unique = true)
    @NotNull
    @Pattern(regexp="^((\\+38)|38)?0\\d{9}$")
    private String phone_number;

    @Column(unique = true)
    @NotNull
    @Pattern(regexp=".+@.+\\..+")
    private String email;

    private String your_business;

    @NotNull
    private String password;

    private Role client;
///////////////////////////////////////
    @OneToMany(mappedBy = "user")
    List<Ads> ads = new ArrayList<>();

    @OneToMany
    private Order order;

}
