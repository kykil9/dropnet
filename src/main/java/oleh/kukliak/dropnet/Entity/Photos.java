package oleh.kukliak.dropnet.Entity;

import javafx.scene.shape.Path;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@ToString
@NoArgsConstructor

@Entity
public class Photos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Path path;

    private String alt;

    private String ads_id;

    @OneToMany(mappedBy = "photos")
    List<Ads> ads = new ArrayList<>();
}
